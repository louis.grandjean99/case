# Product to tech case / Bonx

[[_TOC_]]

## Context

The goal of this exercise is double : 
- Give you insights about how we work at Bonx and, should we decide to work together, contribute to accelerate your onboarding
- Help us understand if we could mutually be a good fit together    
   
   
This document is a Product Brief - a product brief drives the implementation of a feature at Bonx, from its product specification to its tech implementation.   
   
   
Product briefs in Bonx are equivalent to “Pitches”' in the ‘Shape-up’ methodology if you know it (it was invented by Basecamp and used at multiple very successful startups with very high engineering standards such as Alan). We run on a modified Shape-up methodology.   
   
   
This document contains two parts :    
- A snapshot of our product strategy : to get a larger context of why the feature is relevant and how it integrates into a larger purpose strategy.
- The Product brief itself : it details in more details what problems need to be solved, what needs to be built, etc.   
       
About the exercise :
- At this point you should have access to a base repository that’s already functional
- Fork it !
- Exceptionally, you should directly push your commits on main (in real conditions, we work with Feature branches but for the sake of simplicity no need to do this here)
- Feel free to leverage anything you’d use on the job - this means libraries, stackoverflow, etc. But you should be able to explain any bit of code
- The exercice should not take more than 2h
- There is no trap, no fancy stuff, just what the job would be
- You’re building a feature for a no-code platform - keep this in mind, nothing about data or processes can be hardcoded !   
    
What we evaluate
- Your ability to build a working feature that looks and feels professional (hint : about the look, leveraging basic Tailwind classes with no specifics helps a lot).
- Your ability to write clean, simple, readable code.
- Your ability to think of problems first before diving into coding
- Your ability to write robust code, including code that handles error well   
   
## Current product strategy
### Where we are
Our vision on Bonx is that there is hope for manufacturers to completely overturn the ERP model, which is :
- Blackbox tool in the end of external integrators
- Bad UX
- Slow & rigid to evolve
- Standardized processes decorrelated from actual needs, and processes that shift from being functional to non functional and resulting in the ERP churning in the end
- Solo-player games where each individual is alone against the machine
To achieve this, our core focus is to prove and materialize the “self-serve” aspect of the product. This has deep implications such as :
- A power user can autonomously evolve dashboards & processes
- A client can autonomously request new orders
- A “Technical operator” can autonomously chat with the client to solve open issues regarding an order
- etc.    
   
### How to get there
To get there, there are a few major areas in the product that are showing large problems to be solved :
- The data-structure is the fabric on which everything gets built in Bonx. Yet, it remains super painful for power users to define this data structure and evolve it.
- It’s hard for users to understand how we build their processes using a combination of views, forms & automations
- Inputting legacy data (from a Google Sheet or similar) is completely outside the realm of our power users and they completely need us for every slight change in there. Also, we saw a lot of recurring patterns for importing data efficiently and we’re reaching the limits of what should be done manually.
- Self-serve write operations on Bonx data is 0% self-serve - for instance when defining forms & automations and how they should write their data.
- Inputting formula (in views & computed fields in models) works but the UX needs a lot of improvement to work.
- We have not built anything yet regarding customer portals, it looks like it could bring value both for existing clients and acquisition - we have very little insights regarding this yet.
- There are still a few highly requested features on V1 Views that would nearly position this feature as “self-servable” (that is - usable by user but needing a bunch of UX improvements after testing). This includes extending product features to be in a position to kill the majority of custom queries.
- There are quite a few things that we need to simplify in existing client deployments to make the data simple enough to be self-serve (forms with hardcoded list of production steps or suppliers, etc.)
- There are certain industrial use-cases that will need some tech to be implementable   
   
Current priorities
- #4 - implementing “Automations” UI
   
   
## Product Brief
### Pain points we want to address in the feature
- Power users want to define an “automation" or a "workflow” : a linear sequence of actions to launch with a trigger.    
  For now, we focus only on two actions :
   - External API call : a simple action that calls an external API
   - Popup : a simple action that triggers the opening of a popup   
- For now we keep the trigger very simple : The power users can manually launch the automation with a button on the automation page

### User stories
- User can build the automation **with a no-code UI in the frontend**
- User can run the workflow (**in the backend**) with a Run button

### Constraints
- Ensure the code works using the following flow
  - Action #1 : “External API” action with the following parameters   
    URL : https://api.spacexdata.com/v4/payloads/5eb0e4b5b6c3bb0006eeb1e1 (SpaceX API)
  - Action #2 : “Popup action” with the following parameters:   
    Message : “Manufacturer of current payload : {{ steps[0].manufacturers }}”

The data between {{ }} in action #2 should be rendered with data from action #1.
Users should see the popup show up when they launch the automation.

### Solutions 
> To be designed with the candidate during onboarding meeting

## Get started

1. Fork the base repo https://gitlab.com/bbqx/case
2. To get started faster, you can use [Gitpod](https://gitpod.io) which will set up your development environment for you    
   Otherwise, follow these instructions :   
   a. We use Node 18.14.2   
   b. Install turbo globally : `npm install -g turbo`  
   b. Install pnpm   
   c. Install dependencies   
   d. Launch dev server with `pnpm run dev`
   

## Last but not least

When you're done, send us by email at jb@bonx.tech the link to your fork with all the other elements you think are necessary: ​​thoughts, drafts, diagrams, ...     
Good luck and thank you for participating!
