import { build } from "esbuild"
import esbuildPluginPino from "esbuild-plugin-pino"
import { globPlugin } from "esbuild-plugin-glob"

export const options = {
  entryPoints: ["src/**/*.ts"],
  logLevel: "info",
  outdir: "lib",
  bundle: false,
  minify: false,
  platform: "node",
  target: "node18",
  format: "esm",
  sourcemap: true,
  plugins: [
    globPlugin(),
    esbuildPluginPino({ transports: ["pino-pretty"] }),
  ],
}

build(options).catch((e) => {
  console.error(e)
  throw e
})
