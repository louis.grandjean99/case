import { fastify } from "fastify";
import fastifyCors from "@fastify/cors";
import { logger } from "./logger.js";

import automationsPlugin from "./routes/automations.js";

export default function build(options = {}) {
  options = {
    logger,
    ...options,
  };

  const server = fastify(options);

  server.setErrorHandler(async (error, request, reply) => {
    // Logging locally
    logger.error(error);

    reply.status(500).send({ errorMsg: "Something went wrong", error });
  });

  // Enable the fastify CORS plugin
  server.register(fastifyCors, {
    origin: "*",
    methods: "GET,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
  });

  server.get("/", async (request: any, reply: any) => {
    reply.send({ app: "backend", date: new Date() });
  });

  server.post("/run/:id", async (request: any, reply: any) => {
    const id = request.params.id;
    const data = request.body;

    const response = await fetch("https://api.spacexdata.com/v4/payloads/5eb0e4b5b6c3bb0006eeb1e1");
    const result = await response.json();

    reply.send({ message: "Action effectuée avec succès", data: result });
  });

  // Routes
  server.register(automationsPlugin, { prefix: "/automations" });

  return server;
}
